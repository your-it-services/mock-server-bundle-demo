<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    YourITServices\MockServerBundle\MockServerBundle::class => ['dev' => true, 'test' => true],
];
