SHELL=/bin/bash

-include .env
export

LOCAL_USER_ID:=$(shell id -u)
LOCAL_GROUP_ID:=$(shell id -g)
COMPOSE:=docker-compose -f docker/docker-compose.yml
DEMO:=demo
MOCK_API:=mock-api
RUN_AS_ROOT:=$(COMPOSE) exec $(DEMO)
RUN_AS_USER:=$(COMPOSE) exec --user $(LOCAL_USER_ID) $(DEMO)

help: ## This help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

up: ## Setup demo container and dependencies
	@$(COMPOSE) up --detach $(MOCK_API) > /dev/null
	@$(COMPOSE) up --build --detach $(DEMO) > /dev/null
	@$(RUN_AS_USER) composer install --no-scripts
	@$(RUN_AS_USER) bin/console cache:warm

down: ## Stop Docker containers
	@$(COMPOSE) down

load-mocks: ## Load API mocks
	@$(RUN_AS_USER) bin/console mockserver:load

reset-mocks: ## Load API mocks
	@$(RUN_AS_USER) bin/console mockserver:reset

reload-mocks: ## Reload API mocks
	@$(RUN_AS_USER) bin/console mockserver:reset
	@$(RUN_AS_USER) bin/console mockserver:load

rebuild: ## Remove all containers / images and rebuild
	@$(MAKE) nuke
	@$(MAKE) up

nuke: ## Remove all Docker containers / images
	@$(COMPOSE) down --rmi all --volumes
	@rm -rf var
	@rm -rf vendor

shell: ## Shell for running commands on the container
	@$(RUN_AS_USER) bash || true

root-shell: ## Shell for running root commands on the container
	@$(RUN_AS_ROOT) bash || true
